import React from 'react';
import todoItemStyles from '../styles/todo-item.module.css';

function TodoItem () {
    return (
        <div className={todoItemStyles.wrapper}>
            <h3>Item Title</h3>
            <details>item details</details>
            <ul>
                <li>requirement</li>
                <li>requirement</li>
                <li>requirement</li>
            </ul>
        </div>
    )
};

export default TodoItem;
