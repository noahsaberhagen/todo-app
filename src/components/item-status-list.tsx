import React from 'react';
import TodoItem from './todo-item';
import itemStatusListStyles from '../styles/item-status-list.module.css';

function ItemStatusList () {
    return (
        <section className={itemStatusListStyles.section}>
            <h2>Item Status Title</h2>
            <TodoItem />
        </section>
    )
};

export default ItemStatusList;
