import React from 'react';
import boardStyles from "../styles/board.module.css"
import ItemStatusList from './item-status-list';

function Board () {
    return (
        <div className={boardStyles.wrapper}>
            <h1>Board Title</h1>
            <main className={boardStyles.main}>
                <ItemStatusList />
                <ItemStatusList />
                <ItemStatusList />
            </main>
        </div>
    )
};

export default Board;
